package com.huacachi.hibernate_jpa.app;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {
	//PERSISTENCE --> <persistence-unit name="PERSISTENCE"> archivo persisntence.xml
	private static final String PERSISTENCE_UNIT_NAME = "PERSISTENCE";
	//Contiene la conexion hacia la Base de Datos
	private static EntityManagerFactory factory;

	//Este metodo permite obtener la conexion
	public static EntityManagerFactory getEntityManagerFactory() {
		if (factory==null) {
			factory= Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		}
		return factory;				
	}
	
	//Permite cerrar la conexion
	public static void shutdown() {
		if (factory!=null) {
			factory.close();
		}		
	}
}
